package sms

import (
	"testing"
)

func TestSMS(t *testing.T) {
	recipients := []Recipients{
		{
			Msisdn:    "306943157353",
			ForeignID: "",
		},
		{
			Msisdn:    "306951738635",
			ForeignID: "",
		},
		{
			Msisdn:    "306947300987",
			ForeignID: "",
		},
	}

	// viberMessage := Viber{
	// 	Header:        "SuccSteps",
	// 	Tag:           "misel",
	// 	Text:          "Extra text for viber",
	// 	ExpiteText:    "For IOS Only",
	// 	TTL:           60,
	// 	ButtonCaption: "Button Caption",
	// 	ButtonAction:  "Button Action",
	// 	Image:         "https://www.pixelhub.eu/wp-content/uploads/2016/11/Logo_3.png",
	// }
	smsMessage := SMS{
		Header: "SuccSteps",
		Body:   "test paradosi",
	}

	send := SourceInput{
		Channels:       []string{"sms"},
		Recipients:     recipients,
		CallbackDRLURL: "https://www.pixelhub.eu/{foreign_id}",
		// ViberMessage:   viberMessage,
		SMSMessage: smsMessage,
	}
	SendSMS(send)
}
