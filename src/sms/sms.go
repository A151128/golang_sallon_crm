package sms

import (
	"bytes"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type SMS struct {
	Header   string `json:"header"`
	Body     string `json:"body"`
	Encoding string `json:""`
}

type Viber struct {
	Header        string `json:"header"`
	Tag           string `json:"tag"`
	Text          string `json:"text"`
	ExpiteText    string `json:"expiryText"`
	TTL           int    `json:"ttl"`
	ButtonCaption string `json:"buttonCaption"`
	ButtonAction  string `json:"buttonAction"`
	Image         string `json:"image"`
}

type Recipients struct {
	Msisdn    string `json:"msisdn"`
	ForeignID string `json:"foreign_id"`
}

type SourceInput struct {
	Channels       []string     `json:"channels"`
	Recipients     []Recipients `json:"recipients"`
	CallbackDRLURL string       `jsob:"callback_dlr_url"`
	ViberMessage   Viber        `json:"viber"`
	SMSMessage     SMS          `json:"sms"`
}

type SMSMessageResponse struct {
	Status       string `json:"status"`
	ID           string `json:"id"`
	ForeignID    string `json:"foreign_id"`
	Msisdn       string `json:"msisdn"`
	ErrorMessage string `json:"error_message"`
}

type SourceResult struct {
	Status          string               `json:"status"`
	Messages        []SMSMessageResponse `json:"messages"`
	ErrorMessage    string               `json:"error_message"`
	WarningMessages string               `json:"warning_messages"`
}
type DLRS struct {
	ID                             string `json:"id"`
	ForeignID                      string `json:"foreign_id"`
	Msisdn                         string `json:"msisdn"`
	NotificationStatus             string `json:"notification_status"`
	MessageParts                   string `json:"message_parts"`
	IsViber                        string `json:"is_viber"`
	ViberNotificationCode          string `json:"viber_notification_code"`
	ViberNotificationExplain       string `json:"viber_notification_explain"`
	ViberNotificationDatetimeStamp string `json:"viber_notification_datetime_stamp"`
	State                          string `json:"state"`
}

type KapatelRecievedResponse struct {
	Status       string `json:"status"`
	Dlrs         []DLRS `json:"dlrs"`
	ErrorMessage string `json:"error_message"`
}

const (
	apiurl       = "https://api2.smsmobile.gr/receiver_post.php"
	userdetailts = "SuccSteps:SucS3ps236"
)

var myClient = &http.Client{Timeout: 30 * time.Second}

func GetJson(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func SendSMS(send *SourceInput) (*SourceResult, error) {
	var data SourceResult

	b, err := json.Marshal(send)
	if err != nil {
		println(err.Error())

	}
	fmt.Println(string(b))
	var jsonStr = []byte(b)
	req, err := http.NewRequest("POST", apiurl, bytes.NewBuffer(jsonStr))
	if err != nil {

		return &data, err
	}
	sEnc := b64.StdEncoding.EncodeToString([]byte(userdetailts))
	headerstring := "Basic" + " " + sEnc
	fmt.Println(headerstring)
	req.Header.Set("Authorization", headerstring)
	req.Header.Set("Content-Type", "application/json")

	resp, err := myClient.Do(req)
	if err != nil {
		return &data, err
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &data, err
	}

	err = json.Unmarshal([]byte(body), &data)
	if err != nil {
		return &data, err
	}
	fmt.Printf("response Body struct: %+v", data)
	return &data, nil
}
