package mysqldb

import (
	"database/sql"
	"fmt"
	"log"

	"crypto/x509"

	"crypto/tls"
	"io/ioutil"

	"os"

	"github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func Connect() *sql.DB {
	rootCertPool := x509.NewCertPool()
	pem, err := ioutil.ReadFile("server-ca.pem")
	if err != nil {
		println(err.Error())
	}

	if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
		log.Fatal("Failed to append PEM.")
	}

	clientCert := make([]tls.Certificate, 0, 1)

	certs, err := tls.LoadX509KeyPair("client-cert.pem", "client-key.pem")

	if err != nil {
		println(err.Error())
	}

	clientCert = append(clientCert, certs)

	mysql.RegisterTLSConfig("salon", &tls.Config{
		RootCAs:            rootCertPool,
		Certificates:       clientCert,
		InsecureSkipVerify: true,
	})

	connString := os.Getenv("MYSQL_USER") + ":" + os.Getenv("MYSQL_PASS") + "@(" + os.Getenv("MYSQL_HOST") + ")/" + os.Getenv("MYSQL_DB") + "?parseTime=true"
	if os.Getenv("MYSQL_TLS") == "true" {
		connString += "&tls=salon"
	}

	db, err := sql.Open("mysql", connString)

	if err != nil {
		log.Fatal("Could not connect to database")
	}

	err = db.Ping()
	if err != nil {
		log.Fatal("Could not connect to database" + err.Error())
	}

	return db
}

type App struct {
	Router *mux.Router
	DB     *sql.DB
}

func (a *App) Initialize(user, password, dbname string) {
	connectionString := fmt.Sprintf("%s:%s@/%s", user, password, dbname)
	var err error
	a.DB, err = sql.Open("mysql", connectionString)
	if err != nil {
		log.Fatal(err)
	}
	a.Router = mux.NewRouter()
}
