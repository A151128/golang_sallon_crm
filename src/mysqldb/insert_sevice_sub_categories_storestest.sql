DROP TABLE IF EXISTS `service_sub_categories_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_sub_categories_stores` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `service_sub_category_id` INT(11) DEFAULT NULL,
  `store_id` INT(11) DEFAULT NULL,
  `created_at` DATETIME DEFAULT NULL,`service_categories``service_sub_categories`
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),`service_categories`
  KEY `service_sub_category_id` (`service_sub_category_id`),
  KEY `store_id` (`store_id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_`services``service_categories``service_sub_categories``product_stores``hairdressers`sub_categories_stores`
--

LOCK TABLES `service_sub_categories_stores` WRITE;
/*!40000 ALTER TABLE `service_sub_categories_stores` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_sub_categories_stores` ENABLE KEYS */;
UNLOCK TABLES;`product_stores`
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;