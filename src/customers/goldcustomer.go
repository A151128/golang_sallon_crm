package customers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sms"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/keighl/mandrill"
)

var (
	mandrillClient = mandrill.ClientWithKey("5gypQYKoRHkb_4lULcMFsg")
)

type GoldCustomerConfig struct {
	AllGoldCustomers []Customer `json:"data"`
}

type SendGoldCustomerSMS struct {
	Sender            string `json:"sender"`
	MobilePhoneNumber string `json:"mobile_phone_number"`
	Message           string `json:"message"`
}

type SendGoldenEmail struct {
	CustomerName    string `json:"customer_name"`
	Email           string `json:"email"`
	Subject         string `json:"subject"`
	UserEmail       string `json:"user_email"`
	CustomerMessage string `json:"customer_message"`
	CompanyAddress  string `json:"company_address"`
	ContactDetails  string `json:"contact_details"`
	CompanyNickName string `json:"company_nick_name"`
}

var myClient = &http.Client{Timeout: 30 * time.Second}

func AllGoldCustomersJSONPerUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userID, err := strconv.Atoi(vars["id"])
	points, err := strconv.Atoi(vars["points"])

	if err != nil {
		print(err.Error())
		fmt.Fprintf(w, "Not a valid id")
	}
	customers := GetAllCustomersPerUser(userID)
	var goldCustomers []Customer
	for _, g := range customers {
		if g.CustomerPoints >= points {
			goldCustomers = append(goldCustomers, g)
		}
	}
	err = json.NewEncoder(w).Encode(GoldCustomerConfig{AllGoldCustomers: goldCustomers})
	if err != nil {
		println(err.Error())
	}
}
func SendSMSFormGoldCustomersJSON(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var sendGoldSMS SendGoldCustomerSMS
	var sendSMS sms.SourceInput

	err := decoder.Decode(&sendGoldSMS)
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Cannot decode message body "})

	}
	sendSMS.Channels = []string{"sms"}

	sendSMS.Recipients = []sms.Recipients{
		{
			Msisdn: sendGoldSMS.MobilePhoneNumber,
		},
	}

	sendSMS.SMSMessage.Header = sendGoldSMS.Sender
	sendSMS.SMSMessage.Body = sendGoldSMS.Message
	sendSMS.ViberMessage.Header = sendGoldSMS.Sender
	sendSMS.ViberMessage.Tag = sendGoldSMS.Sender
	sendSMS.ViberMessage.Text = sendGoldSMS.Message
	sendSMS.ViberMessage.ExpiteText = sendGoldSMS.Message
	sendSMS.ViberMessage.TTL = 60

	message, err := sms.SendSMS(&sendSMS)
	if err != nil {
		println(err.Error())
	}
	var checkstatus = "https://api2.smsmobile.gr/retrieve_dlr.php?username=SuccSteps&password=SucS3ps236&format=json&ids="
	for _, s := range message.Messages {
		checkstatus = checkstatus + s.ID
	}

	resp, err := myClient.Get(checkstatus)
	if err != nil {
		println(err.Error())
	}
	defer resp.Body.Close()
	kapatelReciptientReposnse := sms.KapatelRecievedResponse{}
	err = sms.GetJson(checkstatus, &kapatelReciptientReposnse)
	if err != nil {
		println(err.Error())
	}

	fmt.Printf("%+v", kapatelReciptientReposnse)

}

func SendEmailForGoldenCustomer(w http.ResponseWriter, r *http.Request) {

	decoder := json.NewDecoder(r.Body)
	var sendEmail SendGoldenEmail
	err := decoder.Decode(&sendEmail)
	if err != nil {
		println(err.Error())
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "Cannot decode message body "})

	}
	messageMandrillHTML := &mandrill.Message{}
	headers := map[string]string{
		"Reply-To": sendEmail.UserEmail,
	}
	fmt.Printf("%+v", sendEmail)

	messageMandrillHTML.AddRecipient(sendEmail.Email, "Info", "to")
	messageMandrillHTML.FromEmail = "adv@succsteps.com"
	messageMandrillHTML.FromName = sendEmail.CompanyNickName + " " + "(" + sendEmail.Email + ")"
	messageMandrillHTML.Subject = sendEmail.Subject
	messageMandrillHTML.InlineCSS = true
	messageMandrillHTML.Headers = headers

	messageHTML := renderGoldenTemplateString(goldenTemplateHTML, sendEmail)
	messageMandrillHTML.HTML = messageHTML
	response, err := mandrillClient.MessagesSend(messageMandrillHTML)

	if err != nil {
		println(err.Error())
		return
	}

	fmt.Println(response[0].Status)
	fmt.Println(response[0].RejectionReason)
	if response[0].Status == "sent" {
		json.NewEncoder(w).Encode(HTTPResp{Status: 200, Description: "You successfully sent the email to: " + sendEmail.Email})
	} else {
		json.NewEncoder(w).Encode(HTTPResp{Status: 500, Description: "An error accurred please try again few minutes: " + sendEmail.Email + " rejection reason: " + response[0].RejectionReason})
	}

}
